# Copyright (C) 2013 OmniROM Project
# Copyright (C) 2012 The CyanogenMod Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


DEVICE_FOLDER := device/mediatek/mtk6589

# inherit from the proprietary version
-include vendor/mediatek/mtk6589/BoardConfigVendor.mk


# board
TARGET_BOARD_PLATFORM := mt6592
TARGET_BOARD_HARDWARE := mt6592
TARGET_ARCH := arm
TARGET_CPU_ABI := armeabi-v7a
TARGET_CPU_ABI2 := armeabi
TARGET_ARCH_VARIANT_FPU := neon
ARCH_ARM_HAVE_NEON := true
ARCH_ARM_HAVE_VFP := true
TARGET_CPU_SMP := true
TARGET_ARCH_VARIANT := armv7-a-neon
TARGET_CPU_VARIANT := cortex-a7

#COMMON_GLOBAL_CFLAGS +=  -mfpu=NEON -mfloat-ABI=softfp
#TARGET_GLOBAL_CPPFLAGS += -mfpu=NEON -mfloat-ABI=softfp

# EGL settings
BOARD_EGL_CFG := $(DEVICE_FOLDER)/egl.cfg
USE_OPENGL_RENDERER := true
#BOARD_EGL_WORKAROUND_BUG_10194508 := true

# kernel
TARGET_PREBUILT_KERNEL := $(DEVICE_FOLDER)/kernel
BOARD_KERNEL_CMDLINE :=
BOARD_KERNEL_PAGESIZE := 2048
BOARD_KERNEL_BASE := 0x10000000
BOARD_FLASH_BLOCK_SIZE := 131072

# partition info
# watch out for the sizes
BOARD_BOOTIMAGE_PARTITION_SIZE := 16291456
BOARD_SYSTEMIMAGE_PARTITION_SIZE := 744488960
BOARD_USERDATAIMAGE_PARTITION_SIZE:= 2147483648
BOARD_RECOVERYIMAGE_PARTITION_SIZE := 16291456

TARGET_USERIMAGES_USE_EXT4 := true
TARGET_USERIMAGES_SPARSE_EXT_DISABLED := true

# recovery
TARGET_RECOVERY_FSTAB := $(DEVICE_FOLDER)/recovery/recovery.fstab
TARGET_PREBUILT_RECOVERY_KERNEL := $(DEVICE_FOLDER)/kernel
BOARD_HAS_NO_SELECT_BUTTON := true
TARGET_RECOVERY_INITRC := $(DEVICE_FOLDER)/recovery/init.rc
DEVICE_RESOLUTION := 1920x1200
TARGET_USE_CUSTOM_LUN_FILE_PATH := "/sys/devices/platform/mt_usb/gadget/lun%d/file"
BOARD_UMS_LUNFILE := "/sys/devices/platform/mt_usb/musb-hdrc.0/gadget/lun%d/file"
BOARD_USE_CUSTOM_RECOVERY_FONT:= \"roboto_15x24.h\"
TARGET_RECOVERY_PIXEL_FORMAT := "RGB_565"

#TWRP
RECOVERY_VARIANT := twrp
BOARD_CUSTOM_RECOVERY_KEYMAPPING := $(DEVICE_FOLDER)/recovery/recovery_keys.c
TW_DEFAULT_EXTERNAL_STORAGE := true
TW_INTERNAL_STORAGE_PATH := "/sdcard"
TW_INTERNAL_STORAGE_MOUNT_POINT := "sdcard"
TW_EXTERNAL_STORAGE_PATH := "/external_sd"
TW_EXTERNAL_STORAGE_MOUNT_POINT := "external_sd"
RECOVERY_GRAPHICS_USE_LINELENGTH := true
TW_NO_SCREEN_TIMEOUT := true
TW_NO_SCREEN_BLANK := true
TW_USE_MODEL_HARDWARE_ID_FOR_DEVICE_ID := true
TWHAVE_SELINUX := true
TW_NO_BATT_PERCENT := true
#TW_NO_USB_STORAGE := true
TW_INCLUDE_FB2PNG := false
#TW_EXCLUDE_SUPERSU := true
#TWRP_EVENT_LOGGING := true
BOARD_HAS_NO_REAL_SDCARD := true
#TW_OEM_BUILD := true
#TW_USE_TOOLBOX := true
TW_NO_REBOOT_BOOTLOADER := true
TW_BRIGHTNESS_PATH  : = /sys/devices/platform/leds-mt65xx/leds/lcd-backlight/brightness
TW_CUSTOM_BATTERY_PATH := /sys/devices/platform/battery/power_supply/battery

BOARD_CUSTOM_BOOTIMG_MK := $(DEVICE_FOLDER)/boot.mk
